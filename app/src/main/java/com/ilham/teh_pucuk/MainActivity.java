package com.ilham.teh_pucuk;

import android.icu.text.Transliterator;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {



//    ImageView;
    ImageButton plusquantity1, plusquantity2, plusquantity3, plusquantity4, plusquantity5, minusquantity1, minusquantity2, minusquantity3, minusquantity4, minusquantity5 ;
    TextView quantitynumber1, quantitynumber2, quantitynumber3, quantitynumber4, quantitynumber5, tv_price_1;
    int quantity1, quantity2, quantity3, quantity4, quantity5;

    /**
     * ini adalah display untuk menampilkan quantity + / -
     */
    private void displayQuantity1() {
        quantitynumber1.setText(String.valueOf(quantity1));
    }
    private void displayQuantity2() {
        quantitynumber2.setText(String.valueOf(quantity2));
    }
    private void displayQuantity3() {
        quantitynumber3.setText(String.valueOf(quantity3));
    }
    private void displayQuantity4() {
        quantitynumber4.setText(String.valueOf(quantity4));
    }
    private void displayQuantity5() {
        quantitynumber5.setText(String.valueOf(quantity5));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View decorView= getWindow().getDecorView();

        int uiOption= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOption);

        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }

        plusquantity1 = findViewById(R.id.addquantity_1);
        minusquantity1 = findViewById(R.id.subquantity_1);
        quantitynumber1 = findViewById(R.id.quantity_1);
        plusquantity1.setOnClickListener(v -> {
            quantity1++;
            displayQuantity1();
        });
        minusquantity1.setOnClickListener(v -> {
            quantity1--;
            displayQuantity1();
        });


        //yang ke 2

        plusquantity2 = findViewById(R.id.addquantity_2);
        minusquantity2 = findViewById(R.id.subquantity_2);
        quantitynumber2 = findViewById(R.id.quantity_2);
        plusquantity2.setOnClickListener(v -> {
            quantity2++;
            displayQuantity2();
        });
        minusquantity2.setOnClickListener(v -> {
            quantity2--;
            displayQuantity2();
        });

        //yang ke 3
        plusquantity3 = findViewById(R.id.addquantity_3);
        minusquantity3 = findViewById(R.id.subquantity_3);
        quantitynumber3 = findViewById(R.id.quantity_3);
        plusquantity3.setOnClickListener(v -> {
            quantity3++;
            displayQuantity3();
        });
        minusquantity3.setOnClickListener(v -> {
            quantity3--;
            displayQuantity3();
        });

        //yang ke 4
        plusquantity4 = findViewById(R.id.addquantity_4);
        minusquantity4 = findViewById(R.id.subquantity_4);
        quantitynumber4 = findViewById(R.id.quantity_4);
        plusquantity4.setOnClickListener(v -> {
            quantity4++;
            displayQuantity4();
        });
        minusquantity4.setOnClickListener(v -> {
            quantity4--;
            displayQuantity4();
        });
            //yang ke 5
        plusquantity5 = findViewById(R.id.addquantity_5);
        minusquantity5 = findViewById(R.id.subquantity_5);
        quantitynumber5 = findViewById(R.id.quantity_5);
        plusquantity5.setOnClickListener(v -> {
            quantity5++;
            displayQuantity5();
        });
        minusquantity5.setOnClickListener(v -> {
            quantity5--;
            displayQuantity5();
        });


    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String  text = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

